defmodule ThousandYearOldVampire.Repo.Migrations.CreateExperiences do
  use Ecto.Migration

  def change do
    create table(:experiences) do
      add :description, :text
      add :placement, :integer

      timestamps()
    end

  end
end
