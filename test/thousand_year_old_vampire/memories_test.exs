defmodule ThousandYearOldVampire.MemoriesTest do
  use ThousandYearOldVampire.DataCase

  alias ThousandYearOldVampire.Memories

  describe "experiences" do
    alias ThousandYearOldVampire.Memories.Experience

    @valid_attrs %{description: "some description", placement: 1}
    @update_attrs %{description: "some updated description", placement: 2}
    @invalid_attrs %{description: nil, placement: nil}

    def experience_fixture(attrs \\ %{}) do
      {:ok, experience} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Memories.create_experience()

      experience
    end

    test "list_experiences/0 returns all experiences" do
      experience = experience_fixture()
      assert Memories.list_experiences() == [experience]
    end

    test "get_experience!/1 returns the experience with given id" do
      experience = experience_fixture()
      assert Memories.get_experience!(experience.id) == experience
    end

    test "create_experience/1 with valid data creates a experience" do
      assert {:ok, %Experience{} = experience} = Memories.create_experience(@valid_attrs)
      assert experience.description == "some description"
      assert experience.placement == 1
    end

    test "create_experience/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Memories.create_experience(@invalid_attrs)
    end

    test "update_experience/2 with valid data updates the experience" do
      experience = experience_fixture()
      assert {:ok, %Experience{} = experience} = Memories.update_experience(experience, @update_attrs)
      assert experience.description == "some updated description"
      assert experience.placement == 2
    end

    test "update_experience/2 validates placement is within an acceptable range" do
      experience = experience_fixture()
      assert {:error, %Ecto.Changeset{errors: error}} = Memories.update_experience(experience, %{@update_attrs | placement: 26})
      assert [{:placement, {"is invalid", [validation: :inclusion, enum: 1..25]}}] = error
    end

    test "update_experience/2 with invalid data returns error changeset" do
      experience = experience_fixture()
      assert {:error, %Ecto.Changeset{}} = Memories.update_experience(experience, @invalid_attrs)
      assert experience == Memories.get_experience!(experience.id)
    end

    test "delete_experience/1 deletes the experience" do
      experience = experience_fixture()
      assert {:ok, %Experience{}} = Memories.delete_experience(experience)
      assert_raise Ecto.NoResultsError, fn -> Memories.get_experience!(experience.id) end
    end

    test "change_experience/1 returns a experience changeset" do
      experience = experience_fixture()
      assert %Ecto.Changeset{} = Memories.change_experience(experience)
    end
  end
end
