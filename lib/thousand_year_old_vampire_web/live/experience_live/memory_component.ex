defmodule ThousandYearOldVampireWeb.MemoryComponent do
  use Phoenix.LiveComponent

  @impl true

  def mount(socket) do
    {:ok, socket}
  end

  @impl true
  def render(assigns) do
    ~L"""
    <div class="memory grid gap-3 p-6 border-solid border-2 rounded-md my-6" id="<%= @memory_id %>">
      <%= @title %>
      <%= for %{description: description, placement: placement} <- @memory do %>
        <div draggable="true" id=<%= placement %> class="experience p-4 bg-gray-700 text-white rounded-md"> <%= description %> </div>
      <% end %>
    </div>
    """
  end
end
