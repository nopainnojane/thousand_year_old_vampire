defmodule ThousandYearOldVampireWeb.ExperienceLive.FormComponent do
  use ThousandYearOldVampireWeb, :live_component

  alias ThousandYearOldVampire.Memories

  @impl true
  def update(%{experience: experience} = assigns, socket) do
    changeset = Memories.change_experience(experience)

    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"experience" => experience_params}, socket) do
    changeset =
      socket.assigns.experience
      |> Memories.change_experience(experience_params)
      |> Map.put(:action, :validate)

    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"experience" => experience_params}, socket) do
    save_experience(socket, socket.assigns.action, experience_params)
  end

  defp save_experience(socket, :edit, experience_params) do
    case Memories.update_experience(socket.assigns.experience, experience_params) do
      {:ok, _experience} ->
        {:noreply,
         socket
         |> put_flash(:info, "Experience updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_experience(socket, :new, experience_params) do
    case Memories.create_experience(experience_params) do
      {:ok, _experience} ->
        {:noreply,
         socket
         |> put_flash(:info, "Experience created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
