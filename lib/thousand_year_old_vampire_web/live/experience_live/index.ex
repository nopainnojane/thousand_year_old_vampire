defmodule ThousandYearOldVampireWeb.ExperienceLive.Index do
  use ThousandYearOldVampireWeb, :live_view

  alias ThousandYearOldVampire.Memories
  alias ThousandYearOldVampire.Memories.Experience

  @impl true
  def mount(_params, _session, socket) do
    experiences = list_experiences()
    memory1 = experiences
    |> Enum.reduce([], fn experience, acc ->
      if experience.placement <= 5, do: [experience | acc], else: acc end)
    |> Enum.reverse

    memory2 = experiences
    |> Enum.reduce([], fn experience, acc ->
      if experience.placement > 5 and experience.placement <= 10, do: [experience | acc], else: acc end)
    |> Enum.reverse

    memory3 = experiences
    |> Enum.reduce([], fn experience, acc ->
      if experience.placement > 10 and experience.placement <= 15, do: [experience | acc], else: acc end)
    |> Enum.reverse

    memory4 = experiences
    |> Enum.reduce([], fn experience, acc ->
      if experience.placement > 15 and experience.placement <= 20, do: [experience | acc], else: acc end)
    |> Enum.reverse

    memory5 = experiences
    |> Enum.reduce([], fn experience, acc ->
      if experience.placement > 20 and experience.placement <= 25, do: [experience | acc], else: acc end)
    |> Enum.reverse

    assigned_socket = socket
    |> assign(:experiences, experiences)
    |> assign(:memory1, memory1)
    |> assign(:memory2, memory2)
    |> assign(:memory3, memory3)
    |> assign(:memory4, memory4)
    |> assign(:memory5, memory5)
    {:ok, assigned_socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    socket
    |> assign(:page_title, "Edit Experience")
    |> assign(:experience, Memories.get_experience!(id))
  end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Experience")
    |> assign(:experience, %Experience{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Experiences")
    |> assign(:experience, nil)
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    experience = Memories.get_experience!(id)
    {:ok, _} = Memories.delete_experience(experience)

    {:noreply, assign(socket, :experiences, list_experiences())}
  end

  defp list_experiences do
    Memories.list_experiences()
  end
end
