defmodule ThousandYearOldVampire.Repo do
  use Ecto.Repo,
    otp_app: :thousand_year_old_vampire,
    adapter: Ecto.Adapters.Postgres
end
