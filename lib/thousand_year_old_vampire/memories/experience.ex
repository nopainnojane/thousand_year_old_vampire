defmodule ThousandYearOldVampire.Memories.Experience do
  use Ecto.Schema
  import Ecto.Changeset

  schema "experiences" do
    field :description, :string
    field :placement, :integer

    timestamps()
  end

  @doc false
  def changeset(experience, attrs) do
    experience
    |> cast(attrs, [:description, :placement])
    |> validate_required([:description, :placement])
    |> validate_inclusion(:placement, 1..25)
  end
end
