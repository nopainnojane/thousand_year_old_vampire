import Sortable from 'sortablejs';

export default {
    mounted() {
        let dragged;

        const hook = this;

        const selector = '#' + this.el.id;

        document.querySelectorAll('.memory').forEach((memory) => {
            new Sortable(memory, {
                animation: 0,
                delay: 50,
                delayOnTouchOnly: true,
                group: 'shared',
                draggable: '.experience',
                ghostClass: 'sortable-ghost'
            });
        });
    }
};
