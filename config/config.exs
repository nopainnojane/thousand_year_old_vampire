# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :thousand_year_old_vampire,
  ecto_repos: [ThousandYearOldVampire.Repo]

# Configures the endpoint
config :thousand_year_old_vampire, ThousandYearOldVampireWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "OaIGjYKUyBuQXA5I1tdykw1hLd/Nn8cft+EB9pgM+4uHHjiiFjEACY8O/iGf10DS",
  render_errors: [view: ThousandYearOldVampireWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: ThousandYearOldVampire.PubSub,
  live_view: [signing_salt: "IsKTG6yw"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
